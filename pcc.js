function parseFormatCodes (text) {
    return String(text)
    .replaceAll("&0", "\x1b[30m")
    .replaceAll("&1", "\x1b[34m")
    .replaceAll("&2", "\x1b[32m")
    .replaceAll("&3", "\x1b[36m")
    .replaceAll("&4", "\x1b[31m")
    .replaceAll("&5", "\x1b[35m")
    .replaceAll("&6", "\x1b[33m")
    .replaceAll("&7", "\x1b[37m")
    .replaceAll("&8", "\x1b[90m")
    .replaceAll("&9", "\x1b[94m")
    .replaceAll("&a", "\x1b[92m")
    .replaceAll("&b", "\x1b[96m")
    .replaceAll("&c", "\x1b[91m")
    .replaceAll("&d", "\x1b[95m")
    .replaceAll("&e", "\x1b[93m")
    .replaceAll("&f", "\x1b[97m")
    .replaceAll("&k", "\x1b[8m")
    .replaceAll("&l", "\x1b[1m")
    .replaceAll("&m", "\x1b[9m")
    .replaceAll("&n", "\x1b[4m")
    .replaceAll("&o", "\x1b[3m")
    .replaceAll("&r", "\x1b[0m")
    + "\x1b[0m";
}

module.exports = { parseFormatCodes };