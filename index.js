// declarations
	const wiki = require("wikipedia");
	const { request } = require("undici");
	const core = require("./core");
	const { parseFormatCodes } = require("./pcc");
	const config = require("./conf");
	const exploits = require("./exploits");
	const mc = require('minecraft-protocol');
	const { MessageBuilder } = require('prismarine-chat')('1.16');
	const registry = require('prismarine-registry')('1.16');
	const ChatMessage = require('prismarine-chat')(registry);
	const prefixes = ['radium:', 'r:', 'r!', 'radium.', 'r.', 'radium!', 'r/', 'radium/', 'rad:', 'rad!', 'rad/', '⁄€‹›ﬁﬂ‡°·‚—±Œ„´‰ˇÁ¨ˆØ∏”’»ÅÍÎÏ˝ÓÔÒÚÆ¸˛Ç◊ı˜Â¯˘¿¡™£¢∞¶•ªº–≠œ∑´®†¥¨ˆøπ“‘«åß∂ƒ©˙∆˚¬…æΩ≈ç√∫˜µ≤≥÷'.split('')].flat();
	const rl = require("readline").createInterface(process.stdin, process.stdout);
	rl.setMaxListeners(2);
	const servers = [{host: "play.kaboom.pw", port: 25565}, {host: "chipmunk.land", port: 25565}]
	const credits = "&4m&c_&6c&e_pla&6y&ce&4r&6 - Creator of bot\n&c_ChipMC_&6 - Help with selfcare\n&4Parker&02991&6 - Wiki and urban command"

// sleep function
	async function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

// hash functions
	host = process.argv[2];
	port = process.argv[3];
	function createTrustedHash () {
		TrustedHash = random(10);
		console.log(parseFormatCodes("&c[&6Hash&c] &c[&6"+host +":"+ port+ "&c] &6Trusted Hash: &c%s"), TrustedHash)
	}
	function createOwnerHash () {
		OwnerHash = random(20);
		console.log(parseFormatCodes("&c[&6Hash&c] &c[&6"+host +":"+ port+ "&c] &6Owner Hash: &c%s"), OwnerHash)
	}

	createTrustedHash()
	createOwnerHash()

// random name function
	function random(length) {
		let result = '';
		const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		const charactersLength = characters.length;
		let counter = 0;
		while (counter < length) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
			counter += 1;
		}
		return result;
	};	
	const escapes = ["\r", "\b", "\f", "\t", "\v", "\x1b"]
	function randomEscape(length) {
		let result = '';
		let counter = 0;
		while (counter < length) {
			result += escapes[Math.floor(Math.random() * escapes.length)];
			counter += 1;
		}
		return result;
	};

// bot creation
async function main (host, port) {
	var bots = [];
		var client = mc.createClient
			(
				{
					host: host,
					port: parseInt(port),
					username: randomEscape(10),
					version: "1.20"
				}
			);

// add core
	client.on("login", async () => {
		core.injectTo(client);
		setInterval(() => {
			client.cmdCore.refillCmdCore();
		}, 60000);
		await sleep(200);
		client.chat(`/extras:prefix &c[&6Prefix: ${prefixes[0]}&c]`);
		await sleep(200);
		client.chat(`/essentials:nickname &6RadiumBot`);
		await sleep(200);
		client.chat(`/commandspy:commandspy on`);
		await sleep(300);
		tellraw("@a", JSON.stringify(MessageBuilder.fromString(credits)) )
	});
	setTimeout(function() {
		client.on("pos", () => {
			client.cmdCore.refillCmdCore()
		})
	}, 5000)

// chat logging
	client.on("playerChat", ({ senderName, plainMessage, unsignedContent, formattedMessage, verified }) => {
		try {
			var msg = new ChatMessage(JSON.parse(unsignedContent))
			console.log(parseFormatCodes("&c[&6Player Chat&c] &c[&6"+host +":"+ port+ "&c] " +msg.toAnsi() ));
		} catch (err) {
			console.log(parseFormatCodes("&c[&6Error&c] &c[&6"+host +":"+ port+ "&c] ") +err)
		}
	})
	client.on("systemChat", async ({ formattedMessage, positionId }) => {
		try {
			var msg = new ChatMessage(JSON.parse(formattedMessage))
			if (!String(msg).startsWith("Command set: ")) {
				console.log(parseFormatCodes("&c[&6System Chat&c] &c[&6"+host +":"+ port+ "&c] ") +msg.toAnsi());
			}
		} catch (err) {
			console.log(parseFormatCodes("&c[&6Error&c] ") + err)
		}
		if (String(msg).startsWith("Successfully disabled CommandSpy")) {
			await sleep(50);
			client.chat("/commandspy:commandspy on");
		} if (String(msg).startsWith("You have been muted")) {
			await sleep(50);
			client.chat("/mute "+client.uuid);
		}
	})

// console chat
	rl.on("line", (line) => {
		if (!String(line).startsWith("§loghash")) {
			client.chat(line);
		} else {
			console.log(parseFormatCodes("&c[&6Hash&c] &c[&6"+host +":"+ port+ "&c] &6Trusted Hash: &c%s"), TrustedHash);
			console.log(parseFormatCodes("&c[&6Hash&c] &c[&6"+host +":"+ port+ "&c] &6Owner Hash: &c%s"), OwnerHash);
		}
	})

// create functions
	function toNBTUUID(uuid) {
		return `[I;${uuid.replace(/-/g, '').match(/.{8}/g).map(str => Number.parseInt(str, 16)).map(num => num & 0x80000000 ? num - 0xffffffff - 1 : num).join(',')}]`;
	}
	function tellraw(players, jsonMessage) {
		client.cmdCore.run(`minecraft:tellraw ${players} ${jsonMessage}`);
	};
	function commandError(players, errorTitle, otherthing) {
		tellraw(players, JSON.stringify
			(
				[
					{
						"text": errorTitle + "\n",
						"color": "red"
					},
					{
						"text": otherthing,
						"color": "gray"
					},
					{
						"text": "<--[HERE]",
						"color": "red",
						"italic": true
					}
				]
			)
		);
	};

//selfcare
	client.on("entity_status", function (packet) {
		if (packet.entityStatus < 24 || packet.entityStatus > 28) return;
		if (packet.entityStatus - 24 <= 0) {
			client.chat("/minecraft:op @s[type=player]")
		}
	})

	client.on('game_state_change', function (packet) {
		if (packet.reason == 3) {
			client.chat("/minecraft:gamemode creative");
		}
	})

// command listener
	const chatListener = function (packet) {
		for (const prefix of prefixes) {
			if (`${packet.plainMessage}`.startsWith(prefix)) {
				const args = `${packet.plainMessage}`.slice(prefix.length).trim().split(/ +/);
				const cmd = args.shift().toLowerCase();

				const cmds = {
					help: async () => {
						tellraw("@a", JSON.stringify(MessageBuilder.fromString("&6Commands &4(&c" + Object.keys(cmds).length + "&4)&6:\n&f" + Object.keys(cmds).join(" "))))
					},
					credits: () => {
						tellraw("@a", JSON.stringify(MessageBuilder.fromString(credits)) )
					},
					echo: async (args) => {
						if (!args.join(" ")) {
							commandError("@a", "Expected text", cmd);
						} else {
							client.chat(args.join(" "));
						};
					},
					refill: async () => {
						client.cmdCore.refillCmdCore()
					},
					cb: async (args) => {
						if (!args.join(" ")) {
							commandError("@a", "Expected text", cmd);
						} else {
							client.cmdCore.run(args.join(" "));
						};
					},
					prefixes: async () => {
						tellraw("@a", JSON.stringify(MessageBuilder.fromString(`&6Prefixes &4(&c${prefixes.length}&4)&6:&f ` + prefixes.join(" "))));
					},
					jsontobcraw: (args) => {
						try {
							var msg = new ChatMessage(JSON.parse(args.join(" ")));
							tellraw("@a", JSON.stringify
								(
									[
										{
											"text": "Your component as Minecraft color codes (click to copy):\n",
											"color": "gold"
										},
										{
											"text": msg.toMotd().replaceAll("§", "&"),
											"color": "white",
											"clickEvent": {
												"action": "copy_to_clipboard",
												"value": msg.toMotd().replaceAll("§", "&")
											},
											"hoverEvent": {
												"action": "show_text",
												"value": "Click to copy!"
											}
										},
										{
											"text": "\n\nDisplayed as:\n",
											"color": "gold"
										},
										JSON.parse(MessageBuilder.fromString("&f" +msg.toMotd().replaceAll("§", "&")))
									]
								)
							)
						} catch (err) {
							commandError("@a", String(err), cmd + " " + args.join(" "));
						}
					},
					getjson: (args) => {
						var msg = JSON.stringify(MessageBuilder.fromString(args.join(" ")))
						try {
							tellraw("@a", JSON.stringify
								(
									[
										{
											"text": "Your component as JSON (click to copy):\n",
											"color": "gold"
										},
										{
											"text": msg,
											"color": "white",
											"clickEvent": {
												"action": "copy_to_clipboard",
												"value": msg
											},
											"hoverEvent": {
												"action": "show_text",
												"value": "Click to copy!"
											}
										}
									]
								)
							)
						} catch (err) {
							commandError("@a", String(err), cmd + " " + args.join(" "));
						}
					},
					wiki: async (args) => {
						// Credits: FNFBoyfriendBot, Parker2991
						try {
							const page = await wiki.page(args.join(' '))
							const summary = await page.intro();
							tellraw(`@a`, JSON.stringify([{ "color": "red", "text": "[" }, { "color": "gold", "text": "Wiki" }, { "color": "red", "text": "] " }, { "text": summary, "color": "gray" }]));
						} catch (error) {
							tellraw(`@a`, JSON.stringify({ "text": error.toString(), "color": "red" }))
						}
					},
					urban: async (args) => {
						// Credits: FNFBoyfriendBot, Parker2991
						let component = [];
						let term = `${args.join(' ')}`
						const query = new URLSearchParams({ term });
						const dictResult = await request(`https://api.urbandictionary.com/v0/define?${query}`);
						const { list } = await dictResult.body.json();
						if (!list.length) {
							tellraw('@a', JSON.stringify({ "text": "No results found", "color": "red" }));
						}
						for (definitions of list) {
							component.push([{ "color": "red", "text": "[" }, { "color": "gold", "text": "Urban" }, { "color": "red", "text": "] " }], [
								{
									text: `${definitions.definition.replaceAll('\r', '').replaceAll('[', '\xa71\xa7n\xa7o').replaceAll(']', '\xa7r\xa77')}\n`,
									color: 'gray',
									underlined: false,
									italic: false,
									translate: "",
									hoverEvent: {
										action: "show_text",
										value: [
											{
												text: `Example \u203a \n ${definitions.example.replaceAll('\r', '').replaceAll('[', '\xa71\xa7n\xa7o').replaceAll(']', '\xa7r\xa77')}\n`,
												color: 'gray'
											},
											{
												text: `Word \u203a ${definitions.word.replaceAll('\r', '').replaceAll('[', '\xa71\xa7n\xa7o').replaceAll(']', '\xa7r\xa77')}\n`,
												color: 'gray',
											},
											{
												text: `Author \u203a ${definitions.author.replaceAll('\r', '').replaceAll('[', '\xa71\xa7n\xa7o').replaceAll(']', '\xa7r\xa77')}\n`,
												color: 'gray'
											},
											{
												text: `written on \u203a ${definitions.written_on.replaceAll('\r', '').replaceAll('[', '\xa71\xa7n\xa7o').replaceAll(']', '\xa7r\xa77')}\n`,
												color: 'gray'
											},
											{
												text: `Rating \u203a Thumbs-Up ${definitions.thumbs_up} / Thumbs-Down ${definitions.thumbs_down}`,
												color: 'gray'
											}
										]
									},
									clickEvent: {
										action: 'open_url',
										value: `${definitions.permalink}`
									}
								},
							])
						}
						tellraw(`@a`, JSON.stringify(component))
					},
					loghash: () => {
						console.log(parseFormatCodes("&c[&6Hash&c] &c[&6"+host +":"+ port+ "&c] &6Trusted Hash: &c%s"), TrustedHash)
						console.log(parseFormatCodes("&c[&6Hash&c] &c[&6"+host +":"+ port+ "&c] &6Owner Hash: &c%s"), OwnerHash)
					},
					crash: (args) => {
						if (args[0] == "" +TrustedHash+ "") {
							client.cmdCore.run(`minecraft:tellraw ${args[1]} ${exploits.crash_translate}`)
							createTrustedHash()
						} else {
							commandError("@a", "Invalid hash", cmd +" "+ args.join(" "))
						}
					},
					kick: (args) => {
						if (args[0] == "" +TrustedHash+ "") {
							client.cmdCore.run(`minecraft:item replace entity ${args[1]} weapon.mainhand with bow{display: {Name: '${exploits.kick_bonkblep}'}}`)
							createTrustedHash()
						} else {
							commandError("@a", "Invalid hash", cmd +" "+ args.join(" "))
						}
					},
					validate: (args) => {
						switch (args[0]) {
							case "" +TrustedHash+ "":
								tellraw("@a[nbt={UUID: " +toNBTUUID(packet.sender)+ "}]", JSON.stringify(MessageBuilder.fromString("&aValid Trusted Hash")))
								break;
						
							case "" +OwnerHash+ "":
								tellraw("@a[nbt={UUID: " +toNBTUUID(packet.sender)+ "}]", JSON.stringify(MessageBuilder.fromString("&aValid Owner Hash")))
								break;

							default:
								tellraw("@a[nbt={UUID: " +toNBTUUID(packet.sender)+ "}]", JSON.stringify(MessageBuilder.fromString("&cInvalid Hash")))
								break;
						}
					},
					eval: (args) => {
					if (args[0] == "" +OwnerHash+ "") {
						try {
							tellraw("@a", JSON.stringify( eval(args.slice(1).join(" ")) ))
						} catch (err) {
							commandError("@a", String(err), cmd + " " +args.join(" "))
						}
						createOwnerHash()
					} else {
						commandError("@a", "Invalid owner hash", cmd + " " +args.join(" "))
					}
					}
				}; if (cmds[cmd]) cmds[cmd](args); else commandError("@a", "Unknown command", cmd) // command executor thingy
			}
		}
	}
client.on("playerChat", chatListener);

client.on("end", () => {
	client.off("playerChat", chatListener);
});

client.on("error", () => {
	main(process.argv[2], process.argv[3])
})
client.on("end", () => {
	main(process.argv[2], process.argv[3])
})
}

main(process.argv[2], process.argv[3]);