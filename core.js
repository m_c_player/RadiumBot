const conf = require("./conf.json");
const vec3 = require("vec3");
const relativePos = new vec3(0, 0, 0);
function injectTo(client) {
    const cmdCore = {
            isCmdCore(pos) {
                return pos.x >= cmdCore.S.x && pos.x <= cmdCore.E.x && pos.y >= cmdCore.S.y && pos.y <= cmdCore.E.y && pos.z >= cmdCore.S.z && pos.z <= cmdCore.E.z;
        },
            run(cmd) {
                relativePos.x++;
                if(relativePos.x >= 16) {
                    relativePos.x = 0;
                    relativePos.y++;
                }
                if(relativePos.y >= conf.cmdCore.layers) {
                    relativePos.y = 0;
                    relativePos.z++;
                }
                if(relativePos.z >= 16) {
                    relativePos.z = 0;
                }
                client.write("update_command_block", {
                    location: {x: cmdCore.S.x+relativePos.x, y: cmdCore.S.y+relativePos.y, z: cmdCore.S.z+relativePos.z},
                    command: cmd,
                    mode: 1,
                    flags: 0b100
                });
            },
            refillCmdCore() {
                client.chat(`/fill ${cmdCore.S.x} ${cmdCore.S.y} ${cmdCore.S.z} ${cmdCore.E.x} ${cmdCore.E.y} ${cmdCore.E.z} minecraft:repeating_command_block{CustomName:'{"text":"Bot core"}'} replace`); 
                client.emit("cmdCore_refilled");
            }
        }
        client.on("position", (pos) => {
            client.pos = pos;
            client.emit("pos", pos);
        });
        client.once("pos", () => {
            client.chat("&7Teleportation Detected, Resetting Core");
            refillCmdCore();
        });

        client.cmdCore = cmdCore;
        return cmdCore;

        function refillCmdCore() {
            cmdCore.S = new vec3(Math.floor(client.pos.x / 16) * 16, 0, Math.floor(client.pos.z / 16) * 16).floor();
            cmdCore.E = cmdCore.S.clone().translate(16, conf.cmdCore.layers, 16).subtract(new vec3(1, 1, 1));
            cmdCore.refillCmdCore();
    }
}

module.exports = {injectTo};